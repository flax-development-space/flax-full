const { EleventyHtmlBasePlugin } = require("@11ty/eleventy");

const markdownIt = require("markdown-it");
const markdownItPandoc = require("markdown-it-pandoc");

const flax = require("./flax-plugins/flax.js");

module.exports = function(eleventyConfig) {
  // add base html to every url
  eleventyConfig.addPlugin(EleventyHtmlBasePlugin);

  eleventyConfig.addPlugin(flax);

  let options = {
    html: true, // Enable HTML tags in source
    linkify: true,
    breaks: true,
  };

  // configure the library with options
  let md = markdownIt(options).use(markdownItPandoc);

  eleventyConfig.setLibrary("md", markdownIt(options));

  return {
    // templates engines : njk
    markdownTemplateEngine: "njk",

    // set the directories
    dir: {
      input: "src",
      output: "public",
      includes: "layouts",
      data: "data",
    },
  };
};
