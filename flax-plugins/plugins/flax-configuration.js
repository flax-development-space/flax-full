// basic configuration for flax

const yaml = require("js-yaml");

const PORT = 8100; // use a port you are reasonably sure is not in use elsewhere

module.exports = function(eleventyConfig) {
  // accept data as yaml
  eleventyConfig.addDataExtension("yaml, yml", (contents) =>
    yaml.load(contents),
  );

  // quiet mode
  eleventyConfig.setQuietMode(true);

  //server
  // only copy file on build. otherwise fake the copy
  eleventyConfig.setServerPassthroughCopyBehavior("passthrough");

  // passthrough file copy //
  eleventyConfig.addPassthroughCopy(
    { "static/tocopy": "/" },
    {
      expand: true,
    },
  );
  eleventyConfig.addPassthroughCopy(
    { "static/images": "/images" },
    {
      expand: true,
    },
  );
};
