/**
 * This shortCode allow calling the data from another collection using the id,
 * to be transformed as a figure with figcaption.
 and transform louvre-voir-figure in links 
*
*  use: 
*
*  {% linkId "id", collections.name  %}
*
*  this in the html will create a link to any collection.
*
*  you need to know the name of the collection you’re callign your file from.
*  worst case scenario, we have a everything collection that has litterally everything .
*  using this will slow the build but will work fine.
 */

module.exports = async function(eleventyConfig) {
  eleventyConfig.addShortcode("linkId", function(id, collection) {
    if (!collection) {
      // eturn ""
      return `<span class="missing-collection">${id}</span>`;
    }

    let item = collection.filter((a) => {
      return a.data?.id?.trim() == id.trim();
    });

    // console.log(item)

    if (!item[0]) {
      return `<span class="missing-id">${id}</span>`;
    }

    let output = `<a href="${item[0].url}" class="wiki">${item[0].data.title}</a>`;

    // try second hand
    // '<a href="' + item[0].url + '" class="wiki">' + item[0].data.title + '</a>';

    return output;
  });
};
