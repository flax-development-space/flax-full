const flaxcss = require("./plugins/flax-css.js");
const flaxaudio = require("./plugins/flax-audio.js");
const flaxsearch = require("./plugins/flax-search.js");
const config = require("./plugins/flax-configuration.js");
const collections = require("./plugins/flax-collections.js");
const parseDate = require("./plugins/flax-date.js");
const markdownify = require("./plugins/flax-markdown.js");
const flaxSlider = require("./plugins/flax-slideshow.js");
const wikiFeats = require("./plugins/flax-linkinside.js");
const flaxdev = require("./plugins/flax-dev-tools.js");
const allmeta = require("./plugins/flax-get-all-meta.js");
const svg = require("./plugins/flax-inline-svg.js");
const imgMgmt = require("./plugins/flax-image-management.js");
const flaxCleaner = require("./plugins/flax-cleaner.js");
const limitData = require("./plugins/limitData.js");
const dateWrangler = require("./plugins/dateWrangler.js");
const markdownifying = require("./plugins/markdownify.js");
const reorderBlock = require("./plugins/reorderBlock.js");
const dejats = require("./plugins/dejats.js");
const groupby = require("./plugins/groupby.js");
const utils = require("./plugins/flax-utils.js");

module.exports = function(eleventyConfig) {
  //flax slider
  eleventyConfig.addPlugin(flaxSlider);

  //dev tools
  eleventyConfig.addPlugin(flaxdev);

  // wiki feats
  eleventyConfig.addPlugin(wikiFeats);

  //image management tools
  eleventyConfig.addPlugin(imgMgmt);

  //svg tools
  eleventyConfig.addPlugin(svg);

  //allmeta tools
  eleventyConfig.addPlugin(allmeta);

  //css
  eleventyConfig.addPlugin(flaxaudio);

  //set base config
  eleventyConfig.addPlugin(config);

  //search
  eleventyConfig.addPlugin(flaxsearch);

  //css
  eleventyConfig.addPlugin(flaxcss);


  //utils & filters
  eleventyConfig.addPlugin(utils)


  // add the date plugins
  eleventyConfig.addPlugin(parseDate);

  // add the markdown options plugins
  eleventyConfig.addPlugin(markdownify);

  // set collections
  eleventyConfig.addPlugin(collections);

  // clean the outputs

  if (process.env.ELEVENTY_RUN_MODE != "serve") {
    eleventyConfig.addPlugin(flaxCleaner);
  }

  // modules from elife thingy

  eleventyConfig.addPlugin(limitData);
  eleventyConfig.addPlugin(dateWrangler);
  eleventyConfig.addPlugin(markdownifying);
  eleventyConfig.addPlugin(reorderBlock);
  eleventyConfig.addPlugin(dejats);
  eleventyConfig.addPlugin(groupby);
};
