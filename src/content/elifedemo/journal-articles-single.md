---
title: The journal
pagination:
  data: neuroscience
  size: 1
  alias: article
permalink: "articles/elife-journal/{{article.id}}/{{article.ver}}/"
layout: "elife/elife-article-single.njk"
---

