---
title: Made with Flax 
permalink: demos/
layout: page/single.njk
menutitle: Demos
order: 900
---

this page contains links for the demos of each flax use:

- a book about [flax](/books/flax101/)
- a journal from [crossref](/24634131/index.html)
- a journal following [elife design principles](/articles/elife-journal/)
- a journal made from [kotahi](#) [coming soon]
- a book about  [ukulele](/books/ukulele/) [coming soon]
