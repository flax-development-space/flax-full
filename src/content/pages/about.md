---
title: About Flax
menutitle: About
layout: page/single.njk
permalink: /about/
order: 100
---




Flax is a set of modules and plugins and tools for the amazing [11ty](https://11ty.dev) managed by the Coko foundation.

- Discussions and help: [Mattermost](https://mattermost.coko.foundation)
- Development: [Gitlab](https://gitlab.coko.foundation/flax/flax/)

