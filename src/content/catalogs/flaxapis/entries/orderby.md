---
title: orderBy
catalog: flaxapis
functionname: orderBy
id: orderBy
intro: Order a collection by a custom api. ascender or descender
order: 1
---

## use case

When you need to sort things in different ways than the basic.

```
% for item in collections[catalog] | orderBy("data.title", "asc") %}
```


## code 

```js
 eleventyConfig.addFilter("orderBy", function(data, meta, order) {
    if (order == "asc") {
      console.log("fun")
      data.sort((a, b) => {
        return a.data[meta] - b.data[meta];
      });
    } else {
      data.sort((a, b) => {
        return b.data[meta] - a.data[meta];
      });
    }
  });
```


this is it
