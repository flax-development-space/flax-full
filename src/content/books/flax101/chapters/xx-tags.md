---
title: working with tags
id: tags
chapternumber: 205
---

The simplest tag system there is, how it works, and how we should manipulate it.
