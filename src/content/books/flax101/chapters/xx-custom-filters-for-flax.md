---
title: catalogue for the filters 
chapternumber: 387
layout: book/chapter.njk
---

Layout catalog that can fetch a custom collections and render it.
This will then make available any items using a filter or a shortcode.
Useful for collections of items you want to show with custom metadata.
