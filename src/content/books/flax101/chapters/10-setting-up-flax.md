---
title: Setting up flax
chapternumber: 1
---

Flax is following this folder system:

Looking at the folders inside a Flax project, you get the set of subfolders and files.

```
├── content
       ├── books
       ├── journals
       ├── pages
       ├── css
       └── add your owns subfolders

├── static
       ├── tocopy
       ├── images
       └── js

├── layouts
       ├── common
       ├── book
       ├── journal
       └── add your own

```

## The `content` subfolder

This is where the content will be. You can put there any file. Eleventy will try to transform it into HTML. The content can be written down in multiple formats, but flax tends to focus on markdown and html for now.

since v.0.2, Flax also convert css files from the `content/css` folder, using the brillant `@11tyrocks/eleventy-plugin-lightningcss`.

{% linkId "adding-content", collections.everything %}

## The `static` subfolder

This is where you’ll put the static assets for your publish. Any static file that need to be added to the site will be put in that folder.

## The `layouts` subfolder

This is where the layouts for all the page will be added. Flax expects nunjucks templates, but any templating languages can be added to Eleventy, and it’ painless most of the time.



