---
title: On css
subtitle: the power of lightning css in the flax world
chapternumber: 200 
---

Flax uses [ LightningCss ](https://lightningcss.dev/docs.html) to genereate the content from the "src" folder.

You can create any css file unless they start with an underscore (`_`) and they will be made available in `/css/` output folder.

In the modern world, we use import in order to get a better developer experience. By default, lightning css will not convert files that start with an underscore. Which is perfect to manage imports in the css and leverage the cascade (and the writing order :D)


Thanks to rob knight for his quite amazing insight.
