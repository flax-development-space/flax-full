---
title: working with date
type: filter and shortcode
chapternumber: 22
---

flax has some built-in tools to work with date.

## getYear(date)

Get the year of a certain date:

```

{% raw %}
{{ getYear() }}
{% endraw %}
```

{% raw %}
`getYear("2022-2-12")` will show "2022"
`getYear()` will write the current year.
{% endraw %}

### renderDate(date, language, monthType)

Render the date using the language set in the function. (english if no info available)
