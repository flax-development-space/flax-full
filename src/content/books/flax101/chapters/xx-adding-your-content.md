---
title: Add your own content to a flax website.
id: adding-content
chapternumber: 12
layout: book/chapter.njk
---


The metadata system:

if the page has a front matter like this:

```
title: xxx
theme: yyy
metaname: metavalue
```

the theme will be `yyy`

The folder can also have a single json file that contains meta data shared by multiple files.
so if there is no meta in the file, it will check the json file.

the `chapters.json` file: 

```
{"theme": "ole"}
```

theme for all the chapter will be `ole`


---

other wise, it will check for the theme in the `site.yml`



