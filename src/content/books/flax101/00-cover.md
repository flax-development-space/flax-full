---
title: Flax 101
permalink: "books/flax101/"
layout: "book/cover.njk"
chapternumber: 0
menutitle: "Flax 101"
class: ""
---

Welcome to flax.
Flax is a set of layouts and themes and plugins for (the amazing) [11ty](https://eleventy.dev) to generate a html and print from diverse content.
It’s developped by Julien Taquet at the [Coko foundation](https://coko.foundation) and is in used in very different places.
Flax is licenced under MIT."

