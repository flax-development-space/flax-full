---
title: The ukulele is fun
authors: wikipedia’s folks
book: ukulele
layout: "book/cover.njk"
chapternumber: 0
permalink: books/ukulele/
---

# Welcome to the ukulele book. You can download the <strong ><a style="font-weight: 800" href='{{"/" | url}}output/uke.pdf'>pdf</a></strong> or the **<a style="font-weight: 800" href='{{"/" | url}}output/uke.epub'>epub</a>**, or read online.

<section id="comp-number-46e5dfb3-847d-4797-9256-74e6eabf1028" class="component-front cover start-right">
<header>
<h1 class="component-title">The ukulele is fun</h1>
</header>
<figure><img src="/images/ukulele/206bef72bdd1_medium.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Credential=editoria%2F20210611%2Fus-east-1%2Fs3%2Faws4_request&amp;X-Amz-Date=20210611T154647Z&amp;X-Amz-Expires=86400&amp;X-Amz-Signature=2fc3ba16c876fa28592ade0ceb8b18e4abcf34e665ba812b8426c5878befe396&amp;X-Amz-SignedHeaders=host"
data-fileid="9b1ccb06-4d2c-49b3-9b19-f76856f059ba">
<figcaption class="decoration">I want to go back to my little grass shack in Kealakekua, Hawaii
I want to be with all the kanes and wahines that I knew long ago I can hear old guitars a
playing, on the beach at Hoonaunau I can hear the Hawaiians saying &quot;Komomai no kaua
ika hale welakahao&quot; It won&apos;t be long &apos;til my ship will be sailing back to
Kona A grand old place that&apos;s always fair to see I&apos;m just a little Hawaiian and
a homeside Island boy I want to go back to my fish and poi I want to go back to my little
grass shack in Kealakekua, Hawaii Where the Humuhumu, Nukunuku a puaa goes swimming by
Where the Humuhumu, Nukunuku a puaa goes swimming by</figcaption>
</figure>
</section>
