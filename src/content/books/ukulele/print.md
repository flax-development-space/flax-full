---
title: "Ukulele is fun"
authors: John D  
book: ukulele
layout: "book/print.njk"
theme: theme-ukulele
type: "cover"
chapternumber: 0
permalink: "/books/ukulele/print/"
coverImage: "/images/ukulele/206bef72bdd1_medium.png"
scripts: 
  - blank.js
---

I want to go back to my little grass shack in Kealakekua, Hawaii I want to be with all the kanes and wahines that I knew long ago I can hear old guitars a playing, on the beach at Hoonaunau I can hear the Hawaiians saying &quot;Komomai no kaua ika hale welakahao&quot; It won&apos;t be long &apos;til my ship will be sailing back to Kona A grand old place that&apos;s always fair to see I&apos;m just a little Hawaiian and a homeside Island boy I want to go back to my fish and poi I want to go back to my little grass shack in Kealakekua, Hawaii Where the Humuhumu, Nukunuku a puaa goes swimming by Where the Humuhumu, Nukunuku a puaa goes swimming by…
