---
pagination:
  data: collections
  size: 1
  alias: tag
title: Tags
layout: tags/tagpage.njk
permalink: /tags/{{ tag }}/
---
